<?php

namespace FrontBundle\Controller;

use CrawlerBundle\Entity\CrawlerParameters;
use CrawlerBundle\Entity\CrawlerResult;
use CrawlerBundle\Entity\SpiderResult;
use CrawlerBundle\Form\CrawlerParametersType;
use CrawlerBundle\Model\Spider;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $crawlerParameters = new CrawlerParameters();
        $crawlerParameters->addHeader('X-Generator');
        $crawlerParameters->addHeader('Cache-Control');
        $crawlerParameters->addHeader('X-Drupal-Cache');
        $crawlerParameters->addHeader('Expires');

        $form = $this->createForm(CrawlerParametersType::class, $crawlerParameters);

        $form->handleRequest($request);

        $modified = array();

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();

            $crawlerParameters = $form->getData();

            $spider = new Spider($crawlerParameters->getMainUrl());

            $spiderResult = $spider->spin($crawlerParameters);

            $crawlerResultRepository = $em->getRepository(CrawlerResult::class);

            $urls = array();
            foreach ($spiderResult->getResults() as $result) {
                $urls[] = $result->getUrl();
            }

            $stored = $crawlerResultRepository->getResultsByUrls($urls);

            $finalResults = new ArrayCollection();
            foreach ($spiderResult->getResults() as $result) {
                $finalItem = $result;
                foreach ($stored as $storedItem) {
                    if ($storedItem->getUrl() == $result->getUrl()) {
                        if ($storedItem->getHeaders() != $result->getHeaders()) {
                            $storedItem->setHeaders($result->getHeaders());
                            $modified[] = $finalItem;
                        }
                        $finalItem = $storedItem;
                    }
                }

                $finalItem->setSpiderResult($spiderResult);
                $finalResults->add($finalItem);
            }

            $spiderResult->setResults($finalResults);


            $em->persist($spiderResult);
            $em->flush();
        }

        return $this->render('FrontBundle:Default:index.html.twig', array(
            "form" => $form->createView(),
            'modified' => $modified
        ));
    }
}
