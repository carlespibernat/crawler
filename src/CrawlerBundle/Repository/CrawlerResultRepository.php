<?php

namespace CrawlerBundle\Repository;

use CrawlerBundle\Entity\CrawlerResult;
use Doctrine\ORM\EntityRepository;

class CrawlerResultRepository extends EntityRepository
{
    /**
     * @param array $urls
     * @return CrawlerResult[]
     */
    public function getResultsByUrls(array $urls)
    {
        return $this->getEntityManager()
            ->createQuery(
                "SELECT c FROM CrawlerBundle:CrawlerResult c WHERE c.url IN (:urls)"
            )
            ->setParameter('urls', $urls)
            ->getResult()
        ;
    }
}