<?php

namespace CrawlerBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;


class CrawlerParameters
{
    /**
     * @Assert\NotBlank()
     * @Assert\Type(type="integer")
     * @Assert\LessThanOrEqual(20)
     * @Assert\GreaterThanOrEqual(0)
     *
     * @var int
     */
    private $depth = 10;

    /**
     * @Assert\NotBlank()
     * @Assert\Url(
     *    message = "The url '{{ value }}' is not a valid url",
     * )
     *
     * @var string
     */
    private $mainUrl = '';

    /**
     * @var ArrayCollection
     */
    private $headers;

    public function __construct()
    {
        $this->headers = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getDepth(): int
    {
        return $this->depth;
    }

    /**
     * @param int $depth
     */
    public function setDepth(int $depth)
    {
        $this->depth = $depth;
    }

    /**
     * @return string
     */
    public function getMainUrl(): string
    {
        return $this->mainUrl;
    }

    /**
     * @param string $mainUrl
     */
    public function setMainUrl(string $mainUrl)
    {
        $this->mainUrl = $mainUrl;
    }

    /**
     * @return ArrayCollection
     */
    public function getHeaders(): ArrayCollection
    {
        return $this->headers;
    }

    /**
     * @param ArrayCollection $headers
     */
    public function setHeaders(ArrayCollection $headers)
    {
        $this->headers = new ArrayCollection();

        foreach ($headers as $header) {
            $this->addHeader($header);
        }
    }

    /**
     * @param string $header
     */
    public function addHeader(string $header)
    {
        $this->headers->add($header);
    }
}