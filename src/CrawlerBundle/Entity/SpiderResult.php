<?php

namespace CrawlerBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="spider_result")
 */
class SpiderResult
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $dateTime;

    /**
     * @var CrawlerResult[]
     * @ORM\OneToMany(targetEntity="CrawlerBundle\Entity\CrawlerResult", mappedBy="crawler_result", cascade={"persist"})
     */
    private $results;

    public function __construct()
    {
        $this->results = new ArrayCollection();
        $this->dateTime = new \DateTime();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return \DateTime
     */
    public function getDateTime(): \DateTime
    {
        return $this->dateTime;
    }

    /**
     * @param \DateTime $dateTime
     */
    public function setDateTime(\DateTime $dateTime)
    {
        $this->dateTime = $dateTime;
    }

    /**
     * @return CrawlerResult[]
     */
    public function getResults()
    {
        return $this->results;
    }

    /**
     * @param CrawlerResult[] $results
     */
    public function setResults(ArrayCollection $results)
    {
        $this->results = new ArrayCollection();

        foreach ($results as $result) {
            $this->addResult($result);
        }
    }

    /**
     * @param CrawlerResult $crawlerResult
     */
    public function addResult(CrawlerResult $crawlerResult)
    {
        $this->results[] = $crawlerResult;
    }


}