<?php

namespace CrawlerBundle\Entity;

use Symfony\Component\DomCrawler\Link;

class LinkStatus
{
    const LINK_NOT_EXISTS = 10;
    const LINK_NOT_CRAWLED = 20;
    const LINK_CRAWLED = 30;
    const LINK_ERROR = 40;

    /**
     * @var string
     */
    private $link;

    /**
     * @var int
     */
    private $status;

    public function __construct()
    {
        $this->setStatus(self::LINK_NOT_EXISTS);
    }

    /**
     * @return string
     */
    public function getLink(): string
    {
        return $this->link;
    }

    /**
     * @param string $link
     */
    public function setLink(string $link)
    {
        $this->link = $link;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus(int $status)
    {
        $this->status = $status;
    }


}