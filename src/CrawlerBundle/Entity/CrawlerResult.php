<?php

namespace CrawlerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="CrawlerBundle\Repository\CrawlerResultRepository")
 * @ORM\Table(name="crawler_result")
 */
class CrawlerResult
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var array
     * @ORM\Column(type="json_array")
     */
    private $urls;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $url;

    /**
     * @var array
     * @ORM\Column(type="json_array")
     */
    private $headers;

    /**
     * @var SpiderResult
     * @ORM\ManyToOne(targetEntity="CrawlerBundle\Entity\SpiderResult", cascade={"persist"})
     * @ORM\JoinColumn(name="spider_id", referencedColumnName="id")
     */
    private $spiderResult;

    public function __construct()
    {
        $this->urls = array();
        $this->headers = array();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return array
     */
    public function getUrls(): array
    {
        return $this->urls;
    }

    /**
     * @param array $urls
     */
    public function setUrls(array $urls)
    {
        $this->urls = $urls;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl(string $url)
    {
        $this->url = $url;
    }

    /**
     * @return string[]
     */
    public function getHeaders(): array
    {
        return $this->headers;
    }

    /**
     * @param string[] $headers
     */
    public function setHeaders(array $headers)
    {
        $this->headers = $headers;
    }

    /**
     * @return SpiderResult
     */
    public function getSpiderResult(): SpiderResult
    {
        return $this->spiderResult;
    }

    /**
     * @param SpiderResult $spiderResult
     */
    public function setSpiderResult(SpiderResult $spiderResult)
    {
        $this->spiderResult = $spiderResult;
    }



    public function __toString()
    {
        return $this->getUrl();
    }
}