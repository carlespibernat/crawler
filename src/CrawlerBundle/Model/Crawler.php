<?php

namespace CrawlerBundle\Model;

use CrawlerBundle\Entity\CrawlerParameters;
use CrawlerBundle\Entity\CrawlerResult;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\DomCrawler\Crawler as DomCrawler;
use Symfony\Component\DomCrawler\Link;

class Crawler
{
    private $links;

    public function __construct()
    {
        $this->links = array();
    }

    /**
     * @param CrawlerParameters $crawlerParameters
     *
     * @return CrawlerResult|string
     */
    public function crawl(CrawlerParameters $crawlerParameters)
    {
        $client = new \GuzzleHttp\Client();

        try {
            $result = $client->request('GET', $crawlerParameters->getMainUrl());

            $links = $this->getLinks($crawlerParameters->getMainUrl(), $result->getBody());

            $headers = array();
            $resultLinks = array();

            foreach ($crawlerParameters->getHeaders() as $header) {
                $headers[] = implode(', ', $result->getHeader($header));
            }

            foreach ($links as $link) {
                $resultLinks[] = $link->getUri();
            }

            $crawlerResult = new CrawlerResult();
            $crawlerResult->setUrl($crawlerParameters->getMainUrl());
            $crawlerResult->setHeaders($headers);
            $crawlerResult->setUrls($resultLinks);

            return $crawlerResult;

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * @param string $rootCrawlerUrl
     * @param string $html
     * @return Link[]
     */
    private function getLinks(string $rootCrawlerUrl, string $html) : array
    {
        $domCrawler = new DomCrawler($html, $rootCrawlerUrl);

        $links = $domCrawler->filter('a')->links();

        return $links;
    }
}