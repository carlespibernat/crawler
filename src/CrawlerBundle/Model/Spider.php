<?php

namespace CrawlerBundle\Model;

use CrawlerBundle\Entity\CrawlerParameters;
use CrawlerBundle\Entity\CrawlerResult;
use CrawlerBundle\Entity\LinkStatus;
use CrawlerBundle\Entity\SpiderResult;
use Doctrine\Common\Collections\ArrayCollection;

class Spider
{

    /**
     * @var LinkStatus[]
     */
    private $linksStatus;

    /**
     * @var UrlUtils
     */
    private $urlUtils;

    /**
     * @var CrawlerResult[]
     */
    private $crawlerResults;

    public function __construct(string $baseUrl)
    {
        $this->linksStatus = array();
        $this->crawlerResults = array();

        $urlUtils = new UrlUtils($this->sanitizeUrl($baseUrl));
        $this->urlUtils = $urlUtils;
    }

    /**
     * @param CrawlerParameters $crawlerParameters
     * @return SpiderResult
     */
    public function spin(CrawlerParameters $crawlerParameters)
    {
        $crawlerParameters->setMainUrl($this->sanitizeUrl($crawlerParameters->getMainUrl()));
        $this->spinRecursive($crawlerParameters);

        $spiderResult = new SpiderResult();

        $spiderResult->setResults(new ArrayCollection($this->crawlerResults));

        return $spiderResult;
    }

    /**
     * @param CrawlerParameters $crawlerParameters
     */
    private function spinRecursive(CrawlerParameters $crawlerParameters)
    {
        if ($status = $this->checkLinkStatus($crawlerParameters->getMainUrl()) != LinkStatus::LINK_CRAWLED) {
            if ($this->checkLinkStatus($crawlerParameters->getMainUrl()) == LinkStatus::LINK_NOT_EXISTS) {
                $this->addLink($crawlerParameters->getMainUrl());
            }

            $crawler = new Crawler();
            $crawlerResult = $crawler->crawl($crawlerParameters);

            $linkStatus = new LinkStatus();
            $linkStatus->setLink($crawlerParameters->getMainUrl());

            if (is_string($crawlerResult)) {
                $this->setLinkStatus($crawlerParameters->getMainUrl(), LinkStatus::LINK_ERROR);
            } else {
                $links = $crawlerResult->getUrls();

                $this->setLinkStatus($crawlerParameters->getMainUrl(), LinkStatus::LINK_CRAWLED);
                $this->crawlerResults[] = $crawlerResult;

                if ($crawlerParameters->getDepth() > 0) {
                    $links = $this->cleanLinksArray($links);
                    foreach ($links as $link) {
                        $newParameters = new CrawlerParameters();
                        $newParameters->setMainUrl($link);
                        $newParameters->setDepth($crawlerParameters->getDepth() - 1);
                        $newParameters->setHeaders($crawlerParameters->getHeaders());

                        $this->spinRecursive($newParameters);
                    }
                }
            }
        }
    }

    /**
     * @param string $link
     */
    private function addLink(string $link)
    {
        $linkStatus = new LinkStatus();
        $linkStatus->setLink($link);

        $this->linksStatus[] = $linkStatus;
    }

    /**
     * @param String[] $links
     *
     * @return string[]
     */
    private function cleanLinksArray(array $links)
    {
        $finalArray = array();

        foreach ($links as $link) {
            $linkUri = $this->sanitizeUrl($link);

            if (
                $this->checkLinkStatus($linkUri) != LinkStatus::LINK_CRAWLED &&
                $this->urlUtils->extractHostFromUrl($linkUri) == $this->urlUtils->getHost()
            ) {
                $finalArray[] = $linkUri;
            }
        }

        return $finalArray;
    }

    /**
     * @param string $url
     * @return mixed|string
     */
    private function sanitizeUrl(string $url) :string
    {
        $url = str_replace('www.', '', $url);
        $url = trim($url, '#');
        $url = trim($url, '/');
        return $url;
    }

    /**
     * @param string $link
     * @return int
     */
    private function checkLinkStatus(string $link): int
    {
        foreach ($this->linksStatus as $linkStatus) {
            if ($linkStatus->getLink() == $link) {
                return $linkStatus->getStatus();
            }
        }

        return LinkStatus::LINK_NOT_EXISTS;
    }

    /**
     * @param string $link
     * @param string $status
     */
    private function setLinkStatus(string $link, string $status)
    {
        foreach ($this->linksStatus as $key => $linkStatus) {
            if ($linkStatus->getLink() == $link) {
                $this->linksStatus[$key]->setStatus($status);

                break;
            }
        }
    }
}