<?php

namespace CrawlerBundle\Model;

class UrlUtils
{
    private $baseUrl;
    private $host;

    public function __construct(string $baseUrl)
    {
        $this->baseUrl = $baseUrl;

        $this->host = $this->extractHostFromUrl($baseUrl);

    }

    public function extractHostFromUrl(string $url)
    {
        $urlData = parse_url($url);

        return isset($urlData['host']) ? $urlData['host'] : false;
    }

    public function getHost(): string
    {
        return $this->host;
    }
}